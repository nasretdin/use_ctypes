import ctypes
from ctypes import *


class Nums(ctypes.Structure):
    _fields_ = [("a", ctypes.c_int),
                ("b", ctypes.c_int)
                ]


if __name__ == '__main__':
    ltest = ctypes.CDLL('./build/libltest.so')
    # in C declaration: int add(int a, int b) BY VALUE
    ltest.add.restype = ctypes.c_int
    ltest.add.argtypes = [ctypes.c_int, ctypes.c_int, ]
    add_result = ltest.add(10, 15)
    print(add_result)

    # int add_p(int* a, int* b) BY POINTER
    ltest.add_p.restype = ctypes.c_int
    ltest.add_p.argtypes = [ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), ]
    add_p_result = ltest.add_p(ctypes.c_int(25), ctypes.c_int(11))
    print(add_p_result)

    # int add_r(int& a, int& b) BY REFERENCE
    ltest.add_r.restype = ctypes.c_int
    add_r_result = ltest.add_r(ctypes.byref(ctypes.c_int(22)), ctypes.byref(ctypes.c_int(33)))
    print(add_r_result)

    #
    # struct Nums{
    #     int a = 0;
    #     int b = 0;
    # };
    # int add_s(Nums nums) STRUCT by VALUE
    nums = Nums(21, 32)
    ltest.add_s.restype = ctypes.c_int
    add_s_result = ltest.add_s(nums)
    print(add_s_result)

    # Point1* get_point(int x, int  y)
    NumWrapper = ctypes.POINTER(Nums)
    ltest.get_point.argtypes = [ctypes.c_int, ctypes.c_int, ]
    ltest.get_point.restype = NumWrapper
    point_ref = ltest.get_point(ctypes.c_int(22), ctypes.c_int(17))
    point = point_ref.contents
    print(point.a, " ", point.b)
    i = 0

