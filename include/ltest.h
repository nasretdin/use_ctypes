#ifndef ltest_h__
#define ltest_h__

struct Point{
    int x = 0;
    int y = 0;
};


struct Point1{
    int x = 0;
    int y = 0;
    Point1(int x, int y){
        this->x = x;
        this->y = y;
    };
    ~Point1(){};
};

/*
extern "C" {
    int add(int, int);
    int add_p(int*, int*);
    int add_r(int&, int&);
    int add_s(Point);
    Point1* get_point(int x, int  y);
}
*/
extern "C" __attribute__ ((visibility("default"))) int add(int, int);
extern "C" int add_p(int*, int*);
extern "C" int add_r(int&, int&);
extern "C" int add_s(Point);
extern "C" Point1* get_point(int x, int  y);
#endif